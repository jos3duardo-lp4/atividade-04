package model;

public class Carro {
		
	String marca;
	String placa;
	String cor;
	String fabricante;
	String ano;
	
	public String getMarca() {
		return marca;
	}
	public String getPlaca() {
		return placa;
	}
	public String getCor() {
		return cor;
	}
	public String getFabricante() {
		return fabricante;
	}
	public String getAno() {
		return ano;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}

	
}
