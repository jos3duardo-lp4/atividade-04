package controller;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import model.Carro;
@SessionScoped
@ManagedBean
public class CarroBean {
	
	private Carro carro;
	private List<Carro> carros;

	public CarroBean() {
		carro =  new Carro();
		carros = new ArrayList<Carro>();
	}

	public Carro getCarro() {
		return carro;
	}

	public List<Carro> getCarros() {
		return carros;
	}

	public void setCarro(Carro carro) {
		this.carro = carro;
	}

	public void setCarros(List<Carro> carros) {
		this.carros = carros;
	}
	
	public void adicionar() {
		carros.add(carro);
		carro = new Carro();
	}
	
}
